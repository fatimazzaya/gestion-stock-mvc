package com.stock.mvc.entites;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;
@Entity
public class Category implements Serializable {
	@Id
	@GeneratedValue
	private long idCategory;
	private String code;
	private String designation;
	
	private List<Article> articles;
	public Category() {
		
	}
	public long getId() {
		return idCategory;
	}

	public void setId(long id) {
		this.idCategory = id;
	}

	public long getIdCategory() {
		return idCategory;
	}

	public void setIdCategory(long idCategory) {
		this.idCategory = idCategory;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public List<Article> getArticles() {
		return articles;
	}

	public void setArticles(List<Article> articles) {
		this.articles = articles;
	}
	
	
}
