package com.stock.mvc.entites;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;
@Entity
public class Fournisseur implements Serializable {
	@Id
	@GeneratedValue
	private long idFournisseur;
	private long idClient;
	private String nom;
	private String prenom;
	private String adresse;
	private String photo;
	private String mail;
	@OneToMany(mappedBy = "fournisseur")
	private List<CommandeFournisseur> commandeFournisseur;
	public Fournisseur() {
		super();
	}

	public long getIdFournisseur() {
		return idFournisseur;
	}

	public void setIdFournisseur(long id) {
		this.idFournisseur = idFournisseur;
	}

	public long getIdClient() {
		return idClient;
	}

	public void setIdClient(long idClient) {
		this.idClient = idClient;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getAdresse() {
		return adresse;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}
	
	
	
}

