package com.stock.mvc.entites;

import java.io.Serializable;

import javax.persistence.*;
@Entity
public class LigneCommandeFournisseur implements Serializable {
	@Id
	@GeneratedValue
	private long idLigneCdeFrs;
	@ManyToOne
	@JoinColumn(name="idArticle")

	
	private Article article;
	@ManyToOne
	@JoinColumn(name="idCommandeFournisseur")
	private CommandeClient commandeFournisseur;

	public long getIdLigneCdeFrs() {
		return idLigneCdeFrs;
	}

	public void setIdLigneCdeFrs(long id) {
		this.idLigneCdeFrs = id;
	}

	public Article getArticle() {
		return article;
	}

	public void setArticle(Article article) {
		this.article = article;
	}

	public CommandeClient getCommandeFournisseur() {
		return commandeFournisseur;
	}

	public void setCommandeFournisseur(CommandeClient commandeFournisseur) {
		this.commandeFournisseur = commandeFournisseur;
	}
	
	
}

