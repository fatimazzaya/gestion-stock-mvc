package com.stock.mvc.entites;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.*;
@Entity
public class MvtStock implements Serializable {
	public static final int ENTREE = 1;
	public static final int SORTIE = 2;
	
	@Id
	@GeneratedValue
	private long idMvtStk;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date dateMvt;
	
	@ManyToOne
	@JoinColumn(name="idArticle")
	private Article article;
	
	private BigDecimal quantite;
	
	private int typeMvt;

	public long getIdMvtStk() {
		return idMvtStk;
	}

	public void setIdMvtStk(long id) {
		this.idMvtStk = id;
	}

	public Date getDateMvt() {
		return dateMvt;
	}

	public void setDateMvt(Date dateMvt) {
		this.dateMvt = dateMvt;
	}

	public Article getArticle() {
		return article;
	}

	public void setArticle(Article article) {
		this.article = article;
	}

	public BigDecimal getQuantite() {
		return quantite;
	}

	public void setQuantite(BigDecimal quantite) {
		this.quantite = quantite;
	}

	public int getTypeMvt() {
		return typeMvt;
	}

	public void setTypeMvt(int typeMvt) {
		this.typeMvt = typeMvt;
	}
	
	
}

